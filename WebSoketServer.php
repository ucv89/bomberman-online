<?php
/**
 * Created by PhpStorm.
 * User: ucv
 * Date: 08-Dec-17
 * Time: 02:03
 */

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

use Controller\ServerController;


require_once __DIR__ . '/src/server/ServerController.php';
require_once __DIR__ . '/vendor/autoload.php';



$wsServer = new WsServer(new ServerController);

$server = IoServer::factory(
    new HttpServer(
        $wsServer
    ), 2612
);

$wsServer->enableKeepAlive($server->loop, 30);

$server->run();