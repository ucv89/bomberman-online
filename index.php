<?php
/**
 * Created by PhpStorm.
 * User: ucv
 * Date: 07-Dec-17
 * Time: 23:03
 */

    require_once 'vendor/autoload.php';

    $loader = new Twig_Loader_Filesystem('templates');
    $twig = new Twig_Environment($loader, array(
        'cache' => false,
    ));


    if(isset($_GET['room']) && $_GET['room'] != ''){
        $params['room'] = ['number' => $_GET['room'] ,'ip' => $_SERVER['SERVER_ADDR']];
        echo $twig->render(
            '/default/views/room.html.twig',
            $params);
        die();
    }else{
        echo $twig->render('/default/views/index.html.twig');
        die();
    }




?>

