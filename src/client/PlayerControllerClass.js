class PlayerController {
    constructor(){
        this._playerId = 0;
        this.playerName = "Player";
    }

    clientAction(action,parameters) {
        //This will be overwriteen so ...
    }

    changeName($name){
        this.clientAction(ACTIONS.CHANGE_NAME,{new_name: $name})
    }

    updatePlayer(data){
        // console.log(data);
        this.playerName = data.name;
    }

    get playerId() {
        return this._playerId;
    }

    set playerId(value) {
        this._playerId = value;
    }
}