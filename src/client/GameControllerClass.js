// import ServerController from 'ServerControllerClass';


var ACTIONS = {
    JOIN_ROOM: "join_room",
    CHANGE_NAME: "change_name",
    SEND_MESSAGE: "send_message",
    PLAYER_ID: "playerId",
    UPDATE_GAME: "updateGame",
};

class GameController {
    constructor(){
        this.ServerController = new ServerController();
        this.PlayerController = new PlayerController();
        this.PlayerController.clientAction = this.clientAction.bind(this);
        this.ServerController.onOpenHook = this.onClientConnected.bind(this);
        this.ServerController.onMessageHook = this.onServerMessage.bind(this);
    }

    connect(url,room_number){
        this.ServerController.connect(url);
        this.roomNumber = room_number;
    }

    onServerMessage(data){
        // console.log(typeof data);
        try {
            var message = JSON.parse(data);
        }catch (err){
            console.log("Invalid json action",data);
            return
        }

        switch (message.action){
            case ACTIONS.PLAYER_ID:{
                this.PlayerController.playerId = message.params.playerId ;
            }break;
            case ACTIONS.UPDATE_GAME:{
                // console.log(message.params);
                if (typeof(message.params.playersData) != "undefined" && message.params.playersData != null){
                    for(let playerId in message.params.playersData){
                        if(playerId == this.PlayerController.playerId){
                            if(message.params.playersData[playerId].name != this.PlayerController.playerName){
                                this.chatMessage("Player["+this.PlayerController.playerId+"] NameChange:" + message.params.playersData[playerId].name);
                            }
                            this.PlayerController.updatePlayer(message.params.playersData[playerId]);
                        }
                    }
                }
            }break;
            case ACTIONS.SEND_MESSAGE:{
                this.chatMessage("["+message.params.from+"]:"+message.params.message );
            }break;

            default:{
                console.log("Message whitout a defined action",message);
            }
        }
    }

    clientAction(action,parameters){
        switch (action){
            case ACTIONS.JOIN_ROOM:{
                this.roomNumber = parameters.room_number;
                this.ServerController.sendMessage('{"action":"'+ACTIONS.JOIN_ROOM+'","params":{"room_number":"'+parameters.room_number+'"}}');
            }break;
            case ACTIONS.CHANGE_NAME:{
                this.ServerController.sendMessage('{"action":"'+ACTIONS.CHANGE_NAME+'","params":{"room_number":"'+this.roomNumber+'","new_name":"'+parameters.new_name+'"}}');
            }break;
            case ACTIONS.SEND_MESSAGE:{
                this.ServerController.sendMessage(JSON.stringify(
                    {
                        "action": action,
                        "params": {
                            parameters
                        }
                    }
                ));
            }break;
            default:{
                console.log("Action["+action+"]:Not defined!");
            }
        }
    }

    chatMessage(message){47
        //This is empty for a reason ...
    }

    onClientConnected(){
        this.clientAction(ACTIONS.JOIN_ROOM,{room_number: this.roomNumber});
        this.chatMessage("Connected to room["+this.roomNumber+"]!");
        this.ServerController.sendMessage('{"action":"'+ACTIONS.CHANGE_NAME+'","params":{"room_number":"'+this.roomNumber+'","new_name":"'+this.PlayerController.playerName+'"}}');
    }
}