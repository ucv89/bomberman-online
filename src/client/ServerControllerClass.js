class ServerController {

    constructor(){
        // this.connect(url);
    }

    connect(url){
        //Creating a connection
        this.url = url;
        this.ws = new WebSocket(this.url);
        //Setting up hooks
        this.ws.onopen = this.onOpen.bind(this);
        this.ws.onmessage = this.onMessage.bind(this);
        this.ws.onclose = this.onClose.bind(this);
        this.ws.onerror = this.onError.bind(this);
    }

    //hooks
    onOpenHook(){}
    onMessageHook ($message){}

    onOpen(e){
        var now = new Date();
        console.log(now + ' Connected to '+ this.url);
        this.onOpenHook()
    }

    onMessage(e){
        var now = new Date();
        console.log(now + ' Message Recived From Server :', e.data);
        this.onMessageHook(e.data);
    }

    onClose(e){
        var now = new Date();
        console.log(now +' Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
        setTimeout(function() {
            this.connect(this.url);
        }.bind(this), 1000)
    }

    onError(e){
        var now = new Date();
        console.error(now + ' Socket encountered error: ', e.message, 'Closing socket')
        console.error(e)
        this.ws.close()
    }

    sendMessage(message){
        console.log()
        this.ws.send(message);
    }
}
