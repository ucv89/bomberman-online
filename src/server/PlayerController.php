<?php
/**
 * Created by PhpStorm.
 * User: ucv
 * Date: 08-Dec-17
 * Time: 16:06
 */

namespace Controller;


class PlayerController
{

    private $playerName;
    private $resourceId;

    public function __construct(int $resourceId)
    {
        $this->playerName = "Player Name";
        $this->resourceId = $resourceId;
    }

    /**
     * @return string
     */
    public function getPlayerName(): string
    {
        return $this->playerName;
    }

    /**
     * @return int
     */
    public function getPlayerId(): int
    {
        return $this->resourceId;
    }

    /**
     * @param string $playerName
     */
    public function setPlayerName(string $playerName): void
    {
        $this->playerName = $playerName;
        echo "[$this->resourceId]Changed name to: ".$this->playerName.PHP_EOL;
    }

    public function getPlayerData(){
        return array(
            'id' => $this->resourceId,
            'name' => $this->playerName,
        );
    }
}