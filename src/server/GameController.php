<?php
/**
 * Created by PhpStorm.
 * User: ucv
 * Date: 08-Dec-17
 * Time: 13:31
 */

namespace Controller;

require_once dirname(dirname(__DIR__)) . '/init.php';
require_once 'PlayerController.php';


use Ratchet\ConnectionInterface;
use Controller\PlayerController;


class GameController
{
    /** @var string  */
    private $roomNumber;

    /** @var \SplObjectStorage  */
    protected $clients;

    /** @var PlayerController[]|Mixed $gameRooms */
    protected $players;

    private $database;

    public function __construct(String $roomNumber = NULL)
    {

        $this->clients = new \SplObjectStorage;
        $this->players = [];

        $this->database = new \Slim\PDO\Database(DB_DNS, DB_USER, DB_PWD);

        if($roomNumber){
            $this->roomNumber = $roomNumber;
            //Init game into that room
        }else{
            $this->roomNumber = substr(md5(microtime()),rand(0,26),5);
            $roomExists = $this->database
                            ->select(array('room_number'))
                            ->from('rooms')
                            ->where('room_number', '=', $this->roomNumber)
                            ->execute()
                            ->fetch();
            if($roomExists){
                echo "Room exists joining".PHP_EOL;
                //Init game into that room
            }else{
                die('No more rooms!'.PHP_EOL);
                $this->database
                        ->insert(array('room_number'))
                        ->into('rooms')
                        ->values(array($this->roomNumber))
                        ->execute();

                echo "Created a room".PHP_EOL;
                //Init game into that room
            }
        }

        echo "Room Initialised[".$this->roomNumber."]".PHP_EOL;

        return $this;
    }

    public function getRoomNumber(){
        return $this->roomNumber;
    }

    public function getClients(){
        return $this->clients;
    }

    public function clientActions($action,ConnectionInterface $from ,$params){
        switch ($action){
            case 'change_name':{
                if(isset($params->new_name) && $params->new_name != ''){
                    /** PlayerController */
                    $this->players[$from->resourceId]->setPlayerName($params->new_name);
                    $this->updatePlayersData();
//                    $from->send('Name Changed!');
                }else{
                    echo "Player[".$this->players[$from->resourceId]->getPlayerName()."][$from->resourceId] cannot change name".PHP_EOL;
                    $from->send('Could not change name!');
                }

            }break;
            case 'send_message':{
                if(isset($params->parameters->message) && $params->parameters->message != ''){
                    echo "[$from->resourceId]".$this->players[$from->resourceId]->getPlayerName().":".$params->parameters->message.PHP_EOL;

                    $message = array(
                        'action' => 'send_message',
                        'params' => [
                            'from' => $this->players[$from->resourceId]->getPlayerName(),
                            'message' => $params->parameters->message
                        ]
                    );
                    /** @var ConnectionInterface $client */

                    foreach ($this->clients as $client) {
                        $client->send(json_encode($message));
                    }
                }
            }break;
            default:{
                echo 'Client Action['.$action.']:not defined!'.PHP_EOL;
            }
        }
    }

    public function getPlayersData(){

        $players = [];
        /** @var PlayerController $player */
        foreach ($this->players as $player) {
            $players[$player->getPlayerId()] = $player->getPlayerData();
        }

        return $players;
    }

    public function getGameData(){
        return array(
            'playersData' => $this->getPlayersData(),
            'boardData' => null,
            'gameData' => null,
        );
    }

    public function disconnectClient (ConnectionInterface $conn) {
        if($this->clients->contains($conn)){
            $this->clients->detach($conn);
            echo $this->players[$conn->resourceId]->getPlayerName()."[".$conn->resourceId."] removed from room [".$this->getRoomNumber()."]".PHP_EOL;
            unset($this->players[$conn->resourceId]);
            $this->updatePlayersData();
            return true;
        }
        return false;
    }

    public function updatePlayersData(){
        /** @var ConnectionInterface $client */
        foreach ($this->clients as $client) {
            $client->send(json_encode(
                array(
                    'action' => 'updateGame',
                    'params' => $this->getGameData(),
                )
            ));
        }
    }

    public function connectClient(ConnectionInterface $conn){
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
        $this->players[$conn->resourceId] = new PlayerController($conn->resourceId);
        $conn->send(json_encode(array(
            'action' => 'playerId',
            'params' => [
                'playerId' => $conn->resourceId,
            ],

        )));

        $this->updatePlayersData();

        echo $this->players[$conn->resourceId]->getPlayerName()." [$conn->resourceId] connected to room [$this->roomNumber]".PHP_EOL;
    }


}