<?php
/**
 * Created by PhpStorm.
 * User: ucv
 * Date: 08-Dec-17
 * Time: 01:58
 */

namespace Controller;

require_once 'vendor/autoload.php';
require_once 'GameController.php';

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;




class ServerController implements MessageComponentInterface
{
    protected $clients;


    /** @var GameController[]|Mixed $gameRooms*/
    protected $gameRooms;

    public function __construct() {
        $this->gameRooms = [];
        $this->clients = new \SplObjectStorage;
        echo "Server Started!\n";
    }



    public function joinRoom(String $roomNumber, ConnectionInterface $playerConnection){

        /** @var GameController $gameRoom */
        foreach ($this->gameRooms as $gameRoom) {
            if($gameRoom->getRoomNumber() == $roomNumber){
                $gameRoom->connectClient($playerConnection);
                return;
            }
        }
        $newGameRoom = new GameController($roomNumber);
        $newGameRoom->connectClient($playerConnection);
        $this->gameRooms[$roomNumber] = $newGameRoom;
    }


    public function onOpen(ConnectionInterface $conn) {
//        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})".PHP_EOL;
    }

    public function onMessage(ConnectionInterface $from, $msg) {

        $message = json_decode($msg);
        $this->handleMessage($from,$message);

    }

    public function handleMessage(ConnectionInterface $from, $message){
        if(isset($message->action)){
            switch ($message->action){
                case 'join_room':{
                    if(isset($message->params)){
                        if(isset($message->params->room_number) && $message->params->room_number != ''){
                            $this->joinRoom($message->params->room_number,$from);
                            $from->send('Connected to room!');
                        }else{
                            echo "join_room whitout room_number".json_encode($message).PHP_EOL;
                        }
                    }else{
                        echo "join_room whitout params".json_encode($message).PHP_EOL;
                    }
                }break;
                case 'change_name':{
                    if(isset($message->params)){
                        if(isset($message->params->room_number)){
                            /** @var GameController */
                            $this->gameRooms[$message->params->room_number]->clientActions('change_name',$from,$message->params);
                        }else{
                            echo "change_name:No room specified!".PHP_EOL;
                        }
                    }
                }break;
                case 'send_message':{
                    $this->gameRooms[$this->findGameroomIdByClient($from)]->clientActions('send_message',$from,$message->params);
                }break;
                default:{
                    echo 'Action without param:'.json_encode($message).PHP_EOL;
                }
            }
        }else{
            echo "Message without action:".json_encode($message).PHP_EOL;
        }
    }

    public function findGameroomIdByClient(ConnectionInterface $from){
        foreach ($this->gameRooms as $gameRoom) {
            /** @var \SplObjectStorage $clients */
            $clients = $gameRoom->getClients();
            if($clients->contains($from)){
                return $gameRoom->getRoomNumber();
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages

        // First Remove the player from the game room
        /** @var GameController $gameRoom */
        foreach ($this->gameRooms as $gameRoom) {
            if($gameRoom->disconnectClient($conn))
            {
                break;
            }
        }

        // Then remove the player from the game
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected".PHP_EOL;
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}